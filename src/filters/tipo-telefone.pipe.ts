import { Pipe, PipeTransform } from '@angular/core';
import { tipoTelefone } from 'src/services/interfaces/tipo-telefone.interface';

@Pipe({ name: 'tipoTelefone' })
export class TipoTelefonePipe implements PipeTransform {
    transform(value: any): any {
        
        return tipoTelefone[value];
    }
}