import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyMaskService } from 'src/services/currency-mask.service';
import { isNumber } from 'util';

@Pipe({ name: 'moneyPtBr' })
export class MoneyRealPipe implements PipeTransform {
    constructor(private currency: CurrencyMaskService) { }
    transform(value: any): any {
        if (!isNumber(value)) {
            value = value.toString();
            value = value.replace(".", "").replace(",", ".")
        }
        else{
            value = value.toString();
        }
        return value;
    }
}