import { Pipe, PipeTransform } from '@angular/core';
import { tipoEmpresa } from 'src/services/interfaces/tipo-empresa.interface';

@Pipe({ name: 'tipoEmpresa' })
export class TipoEmpresaPipe implements PipeTransform {
    transform(value: any): any {
        
        return tipoEmpresa[value];
    }
}