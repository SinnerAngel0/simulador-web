import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyMaskService } from 'src/services/currency-mask.service';
import { isNumber } from 'util';

@Pipe({ name: 'porcentagem' })
export class PorcentagemPipe implements PipeTransform {
    constructor(private currency: CurrencyMaskService) { }
    transform(value: any): any {
        if (isNumber(value)) {
            value = value.toString();
            value = value.replace(".", ",");
            console.log(value)
        }
        else if (!isNaN(value)) {
            value = value.replace(".", ",")
            console.log(value)
        }
        else {
            value = value.toString();
            console.log(value)
        }
        return value;
    }
}