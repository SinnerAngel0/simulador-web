export interface Usuario{
    userName:string,
    fullName:string,
    email:string
    password:string,
    cpf:string,
    tipoTelefone:string,
    telefone:string,
    empresas:Empresa[]
}

export interface Empresa{
    id:number,
    nomeFantasia:string,
    cnpj:string,
    razaoSocial:string,
    produto:string,
    tipoEmpresa:string,
    faturamento:string
}