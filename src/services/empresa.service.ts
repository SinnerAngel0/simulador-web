import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Usuario, Empresa } from './interfaces/usuario.interface';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AppSettings } from './appsettings';

@Injectable({
  providedIn: 'root'
})
export class EmpresaService {
  // private appsettings.defaultUrl = "assets/data/";

  public usuario: Usuario;
  public currentEmpresa;
  appsettings: AppSettings = new AppSettings();
  formModel = this.fb.group({
    NomeFantasia: ['', Validators.required],
    Cnpj: ['', Validators.required],
    RazaoSocial: ['', Validators.required],
    TipoEmpresa: ['', Validators.required],
    Produto: ['', Validators.required],
  })

  constructor(private http: HttpClient, private fb: FormBuilder) {

  }

  //   GetManifest(id): Observable<IManifest> {
  //     return this.http.get<IManifest>(this.appsettings.defaultUrl + id + "/manifest.json");
  //   }

  CadastrarEmpresa(): Observable<Empresa> {
    var body = {
      NomeFantasia: this.formModel.value.NomeFantasia,
      Cnpj: this.formModel.value.Cnpj,
      RazaoSocial: this.formModel.value.RazaoSocial,
      TipoEmpresa: this.formModel.value.TipoEmpresa,
      Produto: this.formModel.value.Produto,
      Faturamento: 0,
    }
    console.log(body)
    return this.http.post<Empresa>(this.appsettings.defaultUrl + "/Empresa/CadastroEmpresa", body);
  }


  // Alteracões

  // AlterarNome(usuario) {
  //   return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarNomeUsuario", usuario);
  // }

  // AlterarEmail(usuario) {
  //   return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarEmailUsuario", usuario);
  // }

  // AlterarTelefone(usuario) {
  //   return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarTelefoneUsuario", usuario);
  // }

  // AlterarSenha(data) {
  //   var resp ={
  //     senha : data.senha,
  //     senhaNova : data.senhaNova
  //   };
  //   return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarSenhaUsuario", resp);
  // }

  // AlterarTipoEmpresa(usuario) {
  //   return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarTipoEmpresaUsuario", usuario);
  // }

  // AlterarRazaoSocialEmpresa(usuario) {
  //   return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarRazaoSocialUsuario", usuario);
  // }

  // AlterarProdutoEmpresa(usuario) {
  //   return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarProdutoUsuario", usuario);
  // }

  // AlterarFaturamentoEmpresa(usuario) {
  //   return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarFaturamentoUsuario", usuario);
  // }


  ErrorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "Server Errror");
  }
}
