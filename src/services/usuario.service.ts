import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Usuario } from './interfaces/usuario.interface';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AppSettings } from './appsettings';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  // private appsettings.defaultUrl = "assets/data/";

  public usuario: Usuario;
  public currentEmpresa;
  appsettings: AppSettings = new AppSettings();
  formModel = this.fb.group({
    FullName: ['', Validators.required],
    Cpf: ['', Validators.required],
    Email: ['', Validators.email],
    Telefone: ['', Validators.required],
    TipoTelefone: [''],
    Passwords: this.fb.group({
      Senha: ['', [Validators.required, Validators.minLength(4)]],
      ConfirmSenha: ['', Validators.required]
    }, { validator: this.comparePasswords }),
  })

  constructor(private http: HttpClient, private fb: FormBuilder) {

  }

  comparePasswords(fb: FormGroup) {
    let confirmPaswrdCtrl = fb.get("ConfirmSenha");
    //passwordMismatch
    //confirmPswrdCtrl.error={passwordMismatch:true}
    if (confirmPaswrdCtrl.errors == null || 'passwordMismatch' in confirmPaswrdCtrl.errors) {
      if (fb.get('Senha').value != confirmPaswrdCtrl.value) {
        confirmPaswrdCtrl.setErrors({ 'passwordMismatch': true });
      }
      else {
        confirmPaswrdCtrl.setErrors(null);
      }
    }
  }

  //   GetManifest(id): Observable<IManifest> {
  //     return this.http.get<IManifest>(this.appsettings.defaultUrl + id + "/manifest.json");
  //   }

  CadastrarUsuario(): Observable<Usuario> {
    var body = {
      FullName: this.formModel.value.FullName,
      Cpf: this.formModel.value.Cpf,
      Senha: this.formModel.value.Passwords.Senha,
      Email: this.formModel.value.Email,
      Telefone: this.formModel.value.Telefone,
      TipoTelefone: this.formModel.value.TipoTelefone,
    }
    return this.http.post<Usuario>(this.appsettings.defaultUrl + "/Usuario/Cadastro", body);
  }

  Login(data) {
    return this.http.post(this.appsettings.defaultUrl + "/Usuario/Login", data);
  }

  GetUserProfile() {
    return this.http.get(this.appsettings.defaultUrl + "/Usuario/BuscarUsuario").toPromise().then((res: Usuario) => {
      this.usuario = res;
      this.currentEmpresa = (res.empresas.length > 0) ? res.empresas[0] : this.currentEmpresa;
    });
  }

  SendPasswordReset(data) {
    return this.http.post(this.appsettings.defaultUrl + "/Usuario/ResetPassword", data);
  }


  SendPasswordChanged(data) {
    var response = {
      Password: data.Passwords.Password,
      Token: data.Token
    }
    return this.http.post(this.appsettings.defaultUrl + "/Usuario/MudarPassword", response);
  }


  // Alteracões

  AlterarNome(usuario) {
    return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarNomeUsuario", usuario);
  }

  AlterarEmail(usuario) {
    return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarEmailUsuario", usuario);
  }

  AlterarTelefone(usuario) {
    return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarTelefoneUsuario", usuario);
  }

  AlterarSenha(data) {
    var resp ={
      senha : data.senha,
      senhaNova : data.senhaNova
    };
    return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarSenhaUsuario", resp);
  }

  AlterarTipoEmpresa(usuario) {
    return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarTipoEmpresaUsuario", usuario);
  }

  AlterarRazaoSocialEmpresa(usuario) {
    return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarRazaoSocialUsuario", usuario);
  }

  AlterarProdutoEmpresa(usuario) {
    return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarProdutoUsuario", usuario);
  }

  AlterarFaturamentoEmpresa(usuario) {
    return this.http.post(this.appsettings.defaultUrl + "/Usuario/AlterarFaturamentoUsuario", usuario);
  }


  ErrorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "Server Errror");
  }
}
