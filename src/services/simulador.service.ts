import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { AppSettings } from './appsettings';

@Injectable({
    providedIn: 'root'
})

export class SimuladorService {
    simulador;
    resultado;
    topicos;
    etapa;
    maturidade = false;
    satisfacao = false;
    active= false;
    saving = false;
    appsettings: AppSettings = new AppSettings();
    visible = true;
    porcentagem = 0;
    constructor(private http: HttpClient) { }


    GetSimulador() {
        return this.http.get(this.appsettings.defaultUrl + "/Simulador/BuscarSimulador").toPromise().then(res => {
            this.simulador = res;
        })
    }
    
    ChangeEmpresa(id){
        return this.http.get(this.appsettings.defaultUrl + "/Simulador/BuscarTabelaEtapa"+this.etapa+"/"+id).toPromise().then((res:any)=>{
            this.topicos = res.campos;
            this.GetTabelaEtapa4(id);
        });
    }

    GetTabelaEtapa1(id) {
        return this.http.get(this.appsettings.defaultUrl + "/Simulador/BuscarTabelaEtapa1/"+id);
    }

    GetTabelaEtapa2(id) {
        return this.http.get(this.appsettings.defaultUrl + "/Simulador/BuscarTabelaEtapa2/"+id);
    }

    GetTabelaEtapa3(id) {
        return this.http.get(this.appsettings.defaultUrl + "/Simulador/BuscarTabelaEtapa3/"+id);
    }

    GetTabelaEtapa4(id) {
        this.saving = true;
        console.log(this.saving)
        return this.http.get(this.appsettings.defaultUrl + "/Simulador/BuscarTabelaEtapa4/"+id).toPromise().then(res=>{
            this.resultado = res;
            console.log(res)
            this.CheckMaturidadeSatisfacao(id);
        });
    }

    GetCampoEmpresa(id) {
        return this.http.get(this.appsettings.defaultUrl + "/Simulador/BuscarCampoEmpresas/" + id);
    }

    SaveCampoEmpresaEtapa1(campoEmpresa) {
        return this.http.post(this.appsettings.defaultUrl + "/Simulador/GravarCampoEmpresaEtapa1", campoEmpresa);
    }

    SaveCampoEmpresaEtapa2(campoEmpresa) {
        return this.http.post(this.appsettings.defaultUrl + "/Simulador/GravarCampoEmpresaEtapa2", campoEmpresa);
    }

    SetCampoEmpresa(data) {
        return this.http.post(this.appsettings.defaultUrl + "/Simulador/CadastrarCampoEmpresa/", data);
    }

    DeleteCampoEmpresa(id) {
        return this.http.get(this.appsettings.defaultUrl + "/Simulador/DeletarCampoEmpresa/" + id);
    }

    CheckMaturidadeSatisfacao(id){
        return this.http.get(this.appsettings.defaultUrl + "/Simulador/CheckMaturidadeSatisfacao/"+id).toPromise().then((res:any)=>{
            console.log(res)
           this.satisfacao = res.satisfacao;
           this.maturidade = res.maturidade;
           this.CheckPorcentagem(id)
        });
    }
    CheckPorcentagem(id){
        return this.http.get(this.appsettings.defaultUrl + "/Simulador/CheckPorcentagem/"+id).toPromise().then((res:any)=>{
            this.saving = false
            console.log(this.saving)
           this.porcentagem = res.porcentagem;
        });
    }

    SendEmail(data, name) {
        var formData = new FormData();
        formData.append("files", data, name);
        return this.http.post(this.appsettings.defaultUrl + "/Simulador/EnviarResultado", formData);
    }
}