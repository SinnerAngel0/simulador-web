import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsuarioService } from 'src/services/usuario.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formModel = {
    Cpf: '',
    Senha: ''
  }
  constructor(private _service: UsuarioService, private authService: AuthService, private toastr: ToastrService) { }

  ngOnInit() { }

  // FilterCnpj(value) {
  //   var cnpj = value + '';
  //   cnpj = cnpj.replace(/\D/g, '');
  //   cnpj = cnpj.replace(/^(\d{2})(\d)/, '$1.$2');
  //   cnpj = cnpj.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3');
  //   cnpj = cnpj.replace(/\.(\d{3})(\d)/, '.$1/$2');
  //   cnpj = cnpj.replace(/(\d{4})(\d)/, '$1-$2');
  //   this.formModel.Cnpj = cnpj;
  // }

  FilterCpf(value) {
    var cpf = value + '';
    cpf = cpf.replace(/\D/g, '');
    cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2");
    cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2");
    cpf = cpf.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
    this.formModel.Cpf = cpf;
  }

  onSubmit(form: NgForm) {
    this._service.Login(form.value).subscribe((res: any) => {
      console.log(res);
      this.authService.CheckAuthorization(res.token);
    }, error => {
      this.toastr.error(error.error.message, "Falha ao tentar logar");
    })
  }

}
