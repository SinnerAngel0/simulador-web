import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppRoutingModule, routingComponents } from './app-routing.module';

import { AppComponent } from './app.component';
import { AuthInterceptor } from './auth/auth.interceptor';

import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { GoogleChartsModule } from 'angular-google-charts';

import { AuthService } from './auth/auth.service';
import { UsuarioService } from 'src/services/usuario.service';
import { CurrencyMaskService } from 'src/services/currency-mask.service';

import { CpfDirective } from 'src/directives/cpf.directive';
import { CnpjDirective } from 'src/directives/cnpj.directive';
import { MenuComponent } from './simulador/menu/menu.component';
import { CurrencyMaskDirective } from 'src/directives/money.directive';

import { TipoEmpresaPipe } from 'src/filters/tipo-empresa.pipe';
import { TipoTelefonePipe } from 'src/filters/tipo-telefone.pipe';
import { MoneyRealPipe } from 'src/filters/real.pipe';
import { ConteudoComponent } from './simulador/diagnostico/conteudo/conteudo.component';
import { CampoTextoComponent } from './simulador/diagnostico/conteudo/campo-texto/campo-texto.component';
import { CampoEmojiComponent } from './simulador/diagnostico/conteudo/campo-emoji/campo-emoji.component';
import { CampoCircleComponent } from './simulador/diagnostico/conteudo/campo-circle/campo-circle.component';
import { ResultadoComponent } from './simulador/diagnostico/resultado/resultado.component';
import { ResultadoPainelComponent } from './simulador/diagnostico/resultado-painel/resultado-painel.component';
import { ValoresViewComponent } from './simulador/diagnostico/valores-view/valores-view.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/pt';
import { GoogleChartComponent } from './simulador/diagnostico/google-chart/google-chart.component';
import { GerenciadorAlteracaoComponent } from './simulador/gerenciador-conta/gerenciador-alteracao/gerenciador-alteracao.component';
import { GerenciarEmpresaComponent } from './simulador/gerenciador-conta/gerenciar-empresa/gerenciar-empresa.component';
import { EmpresaService } from 'src/services/empresa.service';
import { EmpresasComponent } from './simulador/diagnostico/empresas/empresas.component';
import { PorcentagemPanelComponent } from './simulador/diagnostico/porcentagem-panel/porcentagem-panel.component';
import { PorcentagemPipe } from 'src/filters/porcentagem.pipe';
registerLocaleData(localeFr, 'pt');

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,

    CpfDirective,
    CnpjDirective,
    CurrencyMaskDirective,

    TipoEmpresaPipe,
    TipoTelefonePipe,
    MoneyRealPipe,
    PorcentagemPipe,

    MenuComponent,

    ConteudoComponent,
    CampoTextoComponent,
    CampoEmojiComponent,
    CampoCircleComponent,

    ResultadoComponent,
    ResultadoPainelComponent,

    ValoresViewComponent,
    GoogleChartComponent,

    GerenciadorAlteracaoComponent,

    EmpresasComponent,

    PorcentagemPanelComponent,    

  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    GoogleChartsModule.forRoot(),
    ToastrModule.forRoot({
      progressBar: true
    }),
    FontAwesomeModule,
    
  ],
  exports: [CurrencyMaskDirective],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    AuthService,
    UsuarioService,
    EmpresaService,
    CurrencyMaskService 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
