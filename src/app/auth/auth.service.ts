import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthRole } from './auth.roles';

@Injectable({
    providedIn: 'root'
})

export class AuthService{

    constructor(private router : Router){}
    
    CheckAuthorization(token){
        var jwt  = this.ParseJwt(token);
        if(jwt.accessControl == AuthRole.Admin){
            this.router.navigate(["simulador/diagnostico/financas"]);
            localStorage.setItem("token", token);
            return true;
        }
        else if(jwt.accessControl == AuthRole.Usuario){
            this.router.navigate(["simulador/diagnostico/financas"]);
            localStorage.setItem("token", token);
            return true;
        }
        localStorage.removeItem("token");
        this.router.navigate(["home"]);
        return false;
    }

    ParseJwt (token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    
        return JSON.parse(jsonPayload);
    };
}
