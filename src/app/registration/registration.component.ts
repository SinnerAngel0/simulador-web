import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/services/usuario.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(public service: UsuarioService,private toastr:ToastrService,private router :Router) { }


  loading = false;
  ngOnInit() {
    
  }
  FilterCpf(value) {
    var cpf = value + '';
    cpf = cpf.replace(/\D/g, '');
    cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2");
    cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2");
    cpf = cpf.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
    return cpf;
  }

  // FilterCnpj(value) {
  //   var cnpj = value + '';
  //   cnpj = cnpj.replace(/\D/g, '');
  //   cnpj = cnpj.replace(/^(\d{2})(\d)/, '$1.$2');
  //   cnpj = cnpj.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3');
  //   cnpj = cnpj.replace(/\.(\d{3})(\d)/, '.$1/$2');
  //   cnpj = cnpj.replace(/(\d{4})(\d)/, '$1-$2');
  //   return cnpj;
  // }

  onSubmit() {   
      this.loading = true;
      this.service.CadastrarUsuario().subscribe((res:any)=>{
        this.loading = false;
        this.service.formModel.reset();
        this.toastr.success("Seja bem-vindo!","Usuário cadastrado com sucesso!");
        this.router.navigateByUrl("login");
      },(error:any)=>{
        console.log(error)
        this.loading = false;
        error.error.forEach(element => {
          this.toastr.error(element.message,"Falha no registro");          
        })
      })
  }
}
