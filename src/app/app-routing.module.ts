import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SimuladorComponent } from './simulador/simulador.component';
import { LoginComponent } from './login/login.component';
// import { RegistrationComponent } from './registration/registration.component';
import { AuthGuard } from './auth/auth.guard';

import { GerenciadorContaComponent } from './simulador/gerenciador-conta/gerenciador-conta.component';
import { GerenciarNomeComponent } from './simulador/gerenciador-conta/gerenciar-nome/gerenciar-nome.component';
import { GerenciadorPainelComponent } from './simulador/gerenciador-conta/gerenciador-painel/gerenciador-painel.component';
import { GerenciarEmailComponent } from './simulador/gerenciador-conta/gerenciar-email/gerenciar-email.component';
import { GerenciarTelefoneComponent } from './simulador/gerenciador-conta/gerenciar-telefone/gerenciar-telefone.component';
import { GerenciarSenhaComponent } from './simulador/gerenciador-conta/gerenciar-senha/gerenciar-senha.component';
import { GerenciarTipoEmpresaComponent } from './simulador/gerenciador-conta/gerenciar-tipo-empresa/gerenciar-tipo-empresa.component';
import { GerenciarRazaoSocialComponent } from './simulador/gerenciador-conta/gerenciar-razao-social/gerenciar-razao-social.component';
import { GerenciarProdutosComponent } from './simulador/gerenciador-conta/gerenciar-produtos/gerenciar-produtos.component';
// import { GerenciarFaturamentoComponent } from './simulador/gerenciador-conta/gerenciar-faturamento/gerenciar-faturamento.component';
import { HomeComponent } from './home/home.component';
import { DiagnosticoComponent } from './simulador/diagnostico/diagnostico.component';
import { EtapaFinancasComponent } from './simulador/diagnostico/etapa-financas/etapa-financas.component';
import { EtapaCustosComponent } from './simulador/diagnostico/etapa-custos/etapa-custos.component';
import { EtapaProdutoComponent } from './simulador/diagnostico/etapa-produto/etapa-produto.component';
import { GerenciarEmpresaComponent } from './simulador/gerenciador-conta/gerenciar-empresa/gerenciar-empresa.component';
import { RegistrationComponent } from './registration/registration.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegistrationComponent },
  { path: "home", component: HomeComponent },
  {
    path: "simulador", component: SimuladorComponent,
    children: [
      {
        path: "gerenciador-conta", component: GerenciadorContaComponent, children: [
          { path: "painel", component: GerenciadorPainelComponent, canActivate: [AuthGuard] },
          { path: "nome", component: GerenciarNomeComponent, canActivate: [AuthGuard] },
          { path: "email", component: GerenciarEmailComponent, canActivate: [AuthGuard] },
          { path: "telefone", component: GerenciarTelefoneComponent, canActivate: [AuthGuard] },
          { path: "senha", component: GerenciarSenhaComponent, canActivate: [AuthGuard] },
          { path: "tipo-empresa", component: GerenciarTipoEmpresaComponent, canActivate: [AuthGuard] },
          { path: "razao-social", component: GerenciarRazaoSocialComponent, canActivate: [AuthGuard] },
          { path: "produto", component: GerenciarProdutosComponent, canActivate: [AuthGuard] },
          { path: "empresa", component: GerenciarEmpresaComponent, canActivate: [AuthGuard] },

          // { path: "faturamento", component: GerenciarFaturamentoComponent, canActivate: [AuthGuard] }
        ],
        canActivate: [AuthGuard]
      },
      {
        path: "diagnostico", component: DiagnosticoComponent, children: [
          { path: "financas", component: EtapaFinancasComponent,data: { bar: "financas" }, canActivate: [AuthGuard] },
          { path: "custos", component: EtapaCustosComponent,data: { bar: "custos" }, canActivate: [AuthGuard] },
          { path: "produtos", component: EtapaProdutoComponent,data: { bar: "produtos" }, canActivate: [AuthGuard] },
        ], canActivate: [AuthGuard]
      },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  RegistrationComponent,
  LoginComponent,
  SimuladorComponent,
  GerenciadorContaComponent,
  HomeComponent,
  DiagnosticoComponent,
  EtapaFinancasComponent,
  EtapaCustosComponent,
  EtapaProdutoComponent,

  GerenciadorPainelComponent,
  GerenciarNomeComponent,
  GerenciarEmailComponent,
  GerenciarTelefoneComponent,
  GerenciarSenhaComponent,
  GerenciarTipoEmpresaComponent,
  GerenciarRazaoSocialComponent,
  GerenciarProdutosComponent,
  GerenciarEmpresaComponent,
  // GerenciarFaturamentoComponent
]
