import { Component, OnInit, Input, ViewChild, ElementRef, ViewChildren } from '@angular/core';
import { SimuladorService } from 'src/services/simulador.service';
import { UsuarioService } from 'src/services/usuario.service';

@Component({
  selector: 'app-campo-emoji',
  templateUrl: './campo-emoji.component.html',
  styleUrls: ['./campo-emoji.component.css']
})
export class CampoEmojiComponent implements OnInit {

  @Input() opcoes;
  idRandom = this.getRandomInt(0,100);
  active
  constructor(public service: SimuladorService, private uService: UsuarioService) { }
  @ViewChildren("emoji") emoji: any;

  ngOnInit(): void {
    if (this.opcoes[0].resposta) {
      this.active = this.opcoes[0].resposta;
    }
  }
  ngAfterViewInit(): void {
    if (this.opcoes[0].resposta) {
      this.emoji._results[this.opcoes[0].resposta].nativeElement.checked = true;
    }
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  onChange(active) {
    this.active = active;
    this.opcoes[0].resposta = active
    this.service.SaveCampoEmpresaEtapa1(this.opcoes[0]).subscribe(res => {
      this.service.GetTabelaEtapa4(this.uService.currentEmpresa.id);
      console.log(res);
    }, error => {
      console.log(error);
    });
  }


}
