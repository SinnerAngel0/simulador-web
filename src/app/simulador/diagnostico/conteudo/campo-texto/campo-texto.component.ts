import { Component, OnInit, Input } from '@angular/core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { SimuladorService } from 'src/services/simulador.service';
import { UsuarioService } from 'src/services/usuario.service';

declare var $;

@Component({
  selector: 'app-campo-texto',
  templateUrl: './campo-texto.component.html',
  styleUrls: ['./campo-texto.component.css']
})
export class CampoTextoComponent implements OnInit {

  @Input() opcoes;
  constructor(public library: FaIconLibrary, public service: SimuladorService, private uService : UsuarioService) { 
    library.addIconPacks(fas, far);
  }

  ngOnInit(): void {
    // console.log(this.opcoes)
  }

  ngAfterViewInit() {
    // setTimeout(() => {
    //   $('[data-toggle="popover"]').popover();
    //   $('[data-toggle="popover"]').popover('show');
    //   setTimeout(() => {
    //     $('[data-toggle="popover"]').popover("hide");
    //     $('[data-toggle="popover"]').on('hidden.bs.popover', function () {
    //       $('[data-toggle="popover"]').popover("dispose");
    //       // do something...
    //     })
    //   }, 4000)
    // }, 500)

  }

  onAdicionar() {
    this.service.SetCampoEmpresa(this.opcoes[0].campo).subscribe((res: any) => {
      this.opcoes.push(res);
    }, error => {
      console.log(error)
    })
  }
  onRemover(resposta) {
    this.service.DeleteCampoEmpresa(resposta.id).subscribe((res: any) => {
      this.opcoes.splice(this.opcoes.indexOf(resposta), 1);
    }, error => {
      console.log(error)
    })
  }

  saveResposta(resposta) {
    resposta.resposta = resposta.resposta.replace(/[.]/g, "").replace(",", ".");
    this.service.SaveCampoEmpresaEtapa2(resposta).subscribe(res => {
      console.log(res);
      this.service.GetTabelaEtapa4(this.uService.currentEmpresa.id);
    }, error => {
      console.log(error);
    });
  }


}
