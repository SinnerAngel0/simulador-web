import { Component, OnInit, Input, Output, EventEmitter, ViewChildren } from '@angular/core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { SimuladorService } from 'src/services/simulador.service';
import { UsuarioService } from 'src/services/usuario.service';

@Component({
  selector: 'app-campo-circle',
  templateUrl: './campo-circle.component.html',
  styleUrls: ['./campo-circle.component.css']
})
export class CampoCircleComponent implements OnInit {

  @Input() opcoes;
  @Output() respostaEmitter = new EventEmitter();  
  idRandom = this.getRandomInt(0,100);
  constructor(public library: FaIconLibrary, public service : SimuladorService, private uService : UsuarioService) { 
    library.addIconPacks(fas, far);
  }
  @ViewChildren("circle") circle : any;

  ngOnInit(): void {
    if(this.opcoes[0].resposta){
      this.opcoes[0].subCampos[this.opcoes[0].resposta].active =  true;
    }      
  }
  ngAfterViewInit(): void {
    if(this.opcoes[0].resposta){
      console.log(this.circle._results[this.opcoes[0].resposta].nativeElement)
      this.circle._results[this.opcoes[0].resposta].nativeElement.checked =  true;
    }      
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  onChange(active) {
    this.opcoes[0].subCampos.forEach(element => {
      element.active = false;
    });
    this.opcoes[0].subCampos[active].active = true;
    this.opcoes[0].resposta = active;
    this.service.SaveCampoEmpresaEtapa1(this.opcoes[0]).subscribe(res=>{
      this.service.GetTabelaEtapa4(this.uService.currentEmpresa.id);
      console.log(res);
    },error=>{
      console.log(error);      
    });
  }

}
