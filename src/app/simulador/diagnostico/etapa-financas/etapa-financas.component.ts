import { Component, OnInit } from '@angular/core';
import { SimuladorService } from 'src/services/simulador.service';
import { UsuarioService } from 'src/services/usuario.service';

@Component({
  selector: 'app-etapa-financas',
  templateUrl: './etapa-financas.component.html',
  styleUrls: ['./etapa-financas.component.css']
})
export class EtapaFinancasComponent implements OnInit {

  topicos;

  constructor(public service: SimuladorService, private uService : UsuarioService) {}

  ngOnInit(): void {
    this.service.GetTabelaEtapa1(this.uService.currentEmpresa.id).subscribe((res: any) => {
      this.service.GetTabelaEtapa4(this.uService.currentEmpresa.id);
      this.service.topicos = res.campos;
      this.service.etapa = 1;
      // this.conteudos = res.campos[0].subCampos;
      console.log(res);
    });
  }

}
