import { Component, OnInit } from '@angular/core';
import { SimuladorService } from 'src/services/simulador.service';
import { $ } from 'protractor';

@Component({
  selector: 'app-resultado-painel',
  templateUrl: './resultado-painel.component.html',
  styleUrls: ['./resultado-painel.component.css']
})
export class ResultadoPainelComponent implements OnInit {

  active = false;
  icone = [0, 2, 1, 1];
  btnTexto = "Veja seu painel de resultado"
  constructor(public service: SimuladorService) { }

  ngOnInit(): void {
    this.checIsMobile()
    window.addEventListener("resize", () => {
      this.checIsMobile();
    })
  }
  checIsMobile() {
    if (window.innerWidth > 1024) {
      this.btnTexto = "Veja seu painel de resultado"
    }
    else {
      this.btnTexto = "Painel resultados"
    }
  }
  onOpenModal() {
    this.service.active = true;
    document.body.style.overflow = "hidden";
  }

}
