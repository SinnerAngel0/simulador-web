import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-google-chart',
  templateUrl: './google-chart.component.html',
  styleUrls: ['./google-chart.component.css']
})
export class GoogleChartComponent implements OnInit {

  @Input() resposta;
  valor = 0;
  @Input() active = true;
  coloracao = "#dc3545"
  
  chart: any = {
    title: "",
    type: "PieChart",
    roles: [
      { role: 'style', type: 'string', index: 2 }
    ],
    data: [
      ['Copper', 0.3, 'orange'],            // RGB value
      ['Silver', 0.1, '#76A7FA'],            // English color name
      ['Gold', 19.30, 'gold'],
      ['Platinum', 21.45, 'color: #e5e4e2'], // CSS-style declaration
    ],
    columnNames: ["columnTeste2", ""],
    options: {
      pieHole: 0.9,
      pieSliceTextStyle: {
        color: '#14ADCC',
        bold: true,
        fontSize: '100px'
      },
      pieSliceText: 'none',
      legend: 'none',
      pieStartAngle: 0,
      backgroundColor: { fill:'transparent' },
      tooltip: { trigger: 'none' },
      slices: [
        { color: this.coloracao, textStyle: { textAlign: 'center' } },
        { color: 'transparent', textStyle: { color: 'transparent' }, }
      ],

    },
    formatter: [

    ]
  }

  constructor() { }

  ngOnInit(): void {
    this.valor = parseFloat(this.resposta.replace("%", "").replace(",", "."))
    const teste = (this.valor > 0) ? 100 - this.resposta.replace("%", "").replace(",", ".") : 0
     if( this.valor >= 0){
       this.coloracao = "#14ADCC";
       this.chart.options.slices[0].color = "#14ADCC"
      }
      else{
        this.coloracao = "#dc3545";
        this.chart.options.slices[0].color = "#dc3545"
        this.resposta = Math.abs(this.resposta).toString();
     }
     this.chart.data =  [
      ["Maturidade de finanças", { v: this.resposta.replace("%", "").replace(",", ".") / 100, f: this.resposta }, 'orange'],
      ["Maturidade de finanças", { v: teste / 100, f: "" }, 'orange'],
    ];
  }

}
