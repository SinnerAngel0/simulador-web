import { Component, OnInit, Input } from '@angular/core';
import { SimuladorService } from 'src/services/simulador.service';

@Component({
  selector: 'app-porcentagem-panel',
  templateUrl: './porcentagem-panel.component.html',
  styleUrls: ['./porcentagem-panel.component.css']
})
export class PorcentagemPanelComponent implements OnInit {

  @Input() leftPerc = false;
  constructor(public service: SimuladorService) { }

  ngOnInit(): void {
  }

}
