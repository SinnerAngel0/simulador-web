import { Component, OnInit, Input } from '@angular/core';
import { isNumber } from 'util';

@Component({
  selector: 'app-valores-view',
  templateUrl: './valores-view.component.html',
  styleUrls: ['./valores-view.component.css']
})
export class ValoresViewComponent implements OnInit {

  @Input() sufixo = false
  @Input() prefixo = false
  @Input() dinheiro = false
  @Input() preSimbolo = "R$"
  @Input() suSimbolo = "R$"
  @Input() titulo = ""
  @Input() valor: any = "Valor não informado"
  @Input() tipoIcone = 0
  constructor() { }

  ngOnInit(): void {
    
  }

  checkType(value) {
    return typeof value;
  }

}
