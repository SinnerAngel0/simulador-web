import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { SimuladorService } from 'src/services/simulador.service';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import * as jsPDF from 'jspdf';
declare var $: any;
declare let html2canvas: any;


@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.css']
})
export class ResultadoComponent implements OnInit {
  @ViewChild("printable") form: ElementRef;

  a4
  loading = false;
  texto = "Carregando..."

  constructor(public service: SimuladorService, public library: FaIconLibrary) {
    library.addIconPacks(fas, far);
  }

  ngOnInit(): void {
    document.body.classList.add("printing");
    // $('.modal').on('show.bs.modal', ()=> {
    //   $("body").css("overflow", "hidden");
    // });

    // $('.modal').on('hide.bs.modal', ()=> {
    //   // this.show = false;
    //   $("body").css("overflow", "visible");
    // });

    // $('body').on('hidden.bs.modal', '.modal', ()=> {
    //   $(this).removeData('bs.modal');
    // });

    // // Faz o back do browser fechar o modal
    // $(".modal").on("shown.bs.modal", ()=> { // any time a modal is shown
    //   var urlReplace = "#" + $(".modal").attr('id'); // make the hash the id of the modal shown
    //   history.pushState(null, null, urlReplace); // push state that hash into the url
    // });

    // // Esconde a modal no voltar
    // $(window).on('popstate', ()=> {
    //   $(".modal").modal('hide');
    // });
  }

  onClose() {
    this.service.active = false
    document.body.style.overflow = "auto";
  }

  pdfConvert() {
    return new Promise((resolve) => {
      this.form.nativeElement.classList.add("blob");
      this.a4 = [595.28, ((595.28 * this.form.nativeElement.offsetHeight) / this.form.nativeElement.offsetWidth)]; // for a5 size paper width and height

      $('body').scrollTop(0);
      this.getCanvas(this.form.nativeElement).then((canvas) => {
        resolve(canvas);
      });
    })
  }

  onCompartilhar() {
    this.loading = true;
    this.texto = "Enviando E-mail..."
    this.pdfConvert().then((canvas: any) => {
      var
        img = canvas.toDataURL("image/png", 1.0),
        doc = new jsPDF("p", "px", this.a4, true);

      doc.addImage(img, 'png', 0, 0, 445, (445 * this.form.nativeElement.offsetHeight) / this.form.nativeElement.offsetWidth, '', 'FAST');
      var blobPDF = new Blob([doc.output('blob')], { type: "application/pdf" });
      this.service.SendEmail(blobPDF, "diagnostico_financeiro.pdf").subscribe((res: any) => {
        console.log(res);
        this.loading = false;
      })
      this.form.nativeElement.classList.remove("blob");
    })

  }
  getCanvas(form) {
    return html2canvas(form, {

    });
  }
  onDownload() {
    this.loading = true;
    this.texto = "Gerando PDF..."
    this.pdfConvert().then((canvas: any) => {
      var
        img = canvas.toDataURL("image/png", 1.0),
        doc = new jsPDF("p", "px", this.a4, true);
      doc.addImage(img, 'png', 0, 0, 445, (445 * this.form.nativeElement.offsetHeight) / this.form.nativeElement.offsetWidth, '', 'FAST');
      doc.save('diagnostico_financeiro.pdf');
      this.form.nativeElement.classList.remove("blob");
      this.loading = false;
    })
  }

  onImprimir() {
    window.addEventListener('beforeprint', (event) => {
      document.body.classList.add("printing")
    });
    window.addEventListener('afterprint', (event) => {
      document.body.classList.remove("printing")
    });
    window.print()
  }

}
