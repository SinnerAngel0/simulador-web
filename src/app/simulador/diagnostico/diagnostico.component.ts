import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UsuarioService } from 'src/services/usuario.service';
import { SimuladorService } from 'src/services/simulador.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-diagnostico',
  templateUrl: './diagnostico.component.html',
  styleUrls: ['./diagnostico.component.css']
})
export class DiagnosticoComponent implements OnInit {

  constructor(public service: SimuladorService, public uService: UsuarioService, private route: ActivatedRoute, private router : Router) { }
  @ViewChild("resultado") resultado: ElementRef;
  currentPath;
  path = ["financas","custos","produtos"]
  index = 0;

  ngOnInit(): void {
    console.log(this.uService.usuario)
    this.route.url.subscribe(() => {
      this.currentPath = this.route.snapshot.firstChild.data.bar;
      this.index = this.path.findIndex((element)=>{return element == this.currentPath });
      console.log(this.index)
    });
  }
  

  onBack(){
    var index = this.path.findIndex((element)=>{return element == this.currentPath})
    this.router.navigate(["simulador/diagnostico/"+ this.path[index - 1]]);
    window.scrollTo(0,0);
  }
  
  onNext(){
    var index = this.path.findIndex((element)=>{return element == this.currentPath})
    this.router.navigate(["simulador/diagnostico/"+this.path[index + 1]]);
    window.scrollTo(0,0);

  }

}
