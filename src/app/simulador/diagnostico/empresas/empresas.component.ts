import { Component, OnInit } from '@angular/core';
import { SimuladorService } from 'src/services/simulador.service';
import { UsuarioService } from 'src/services/usuario.service';

@Component({
  selector: 'app-empresas',
  templateUrl: './empresas.component.html',
  styleUrls: ['./empresas.component.css']
})
export class EmpresasComponent implements OnInit {

  constructor(public service: SimuladorService, public uService: UsuarioService) { }

  ngOnInit(): void {
  }

  onChange() {
    this.service.ChangeEmpresa(this.uService.currentEmpresa.id);
  }

}
