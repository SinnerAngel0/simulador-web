import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/services/usuario.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-gerenciar-produtos',
  templateUrl: './gerenciar-produtos.component.html',
  styleUrls: ['./gerenciar-produtos.component.css']
})
export class GerenciarProdutosComponent implements OnInit {

  constructor(public uService: UsuarioService, private router: Router,private toastr:ToastrService) {}

    ngOnInit(): void {
    }

    onSalvar(event) {
        this.uService.usuario = event;
        this.uService.AlterarProdutoEmpresa(this.uService.usuario).subscribe((res:any) => {
            this.uService.GetUserProfile();
            console.log(res)
            this.toastr.success(res.data);
            this.router.navigate(["simulador/gerenciador-conta/painel"]);
        }, error => {
            console.log(error);
            this.toastr.error("Não foi possivel trocar o produto");
        })
    }

}
