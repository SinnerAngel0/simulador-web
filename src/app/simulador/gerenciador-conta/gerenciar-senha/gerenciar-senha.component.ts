import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/services/usuario.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-gerenciar-senha',
  templateUrl: './gerenciar-senha.component.html',
  styleUrls: ['./gerenciar-senha.component.css']
})
export class GerenciarSenhaComponent implements OnInit {

  constructor(public uService: UsuarioService, private router: Router,private toastr:ToastrService) { }

  trocaSenha = {
    senha: "",
    senhaNova: "",
    confirmSenhaNova: ""
  }
  ngOnInit(): void {
  }

  onSalvar(event) {
    if (this.trocaSenha.senhaNova == this.trocaSenha.confirmSenhaNova) {
      this.uService.AlterarSenha(this.trocaSenha).subscribe((res:any) => {
        this.uService.GetUserProfile();
        this.toastr.success(res.data);
        console.log(res)
        this.router.navigate(["simulador/gerenciador-conta/painel"]);
      }, error => {
        this.toastr.error("Não foi possivel trocar a senha");
        console.log(error);
      })
    }
    else{
      this.toastr.error("Erro ao confirmar a nova senha");
    }
  }

}
