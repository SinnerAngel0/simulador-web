import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/services/usuario.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-gerenciar-razao-social',
  templateUrl: './gerenciar-razao-social.component.html',
  styleUrls: ['./gerenciar-razao-social.component.css']
})
export class GerenciarRazaoSocialComponent implements OnInit {

  constructor(public uService: UsuarioService, private router: Router,private toastr:ToastrService) {}

    ngOnInit(): void {
    }

    onSalvar(event) {
        this.uService.usuario = event;
        this.uService.AlterarRazaoSocialEmpresa(this.uService.usuario).subscribe((res:any) => {
            this.uService.GetUserProfile();
            console.log(res)
            this.toastr.success(res.data);
            this.router.navigate(["simulador/gerenciador-conta/painel"]);
        }, error => {
            console.log(error);
            this.toastr.error("Não foi possivel trocar a razão social");
        })
    }

}
