import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { EmpresaService } from 'src/services/empresa.service';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { UsuarioService } from 'src/services/usuario.service';

@Component({
  selector: 'app-gerenciar-empresa',
  templateUrl: './gerenciar-empresa.component.html',
  styleUrls: ['./gerenciar-empresa.component.css']
})
export class GerenciarEmpresaComponent implements OnInit {

  loading = false;
  
  constructor(private uService : UsuarioService, public service: EmpresaService,private toastr:ToastrService,private router :Router,library: FaIconLibrary) { 
    library.addIconPacks(fas, far);
  }

  ngOnInit(): void {
  }
  
  onSubmit() {   
    this.loading = false;
    this.service.CadastrarEmpresa().subscribe((res:any)=>{
      this.loading = true;
      this.service.formModel.reset();
      this.toastr.success("Empresa cadastrada com sucesso!");
      this.uService.GetUserProfile();   
      this.router.navigateByUrl("simulador/diagnostico/financas");
    },(error:any)=>{
      console.log(error)
      error.error.forEach(element => {
        this.toastr.error(element.message,"Falha no registro");          
      })
    })
}

FilterCnpj(value) {
  var cnpj = value + '';
  cnpj = cnpj.replace(/\D/g, '');
  cnpj = cnpj.replace(/^(\d{2})(\d)/, '$1.$2');
  cnpj = cnpj.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3');
  cnpj = cnpj.replace(/\.(\d{3})(\d)/, '.$1/$2');
  cnpj = cnpj.replace(/(\d{4})(\d)/, '$1-$2');
  return cnpj;
}

}
