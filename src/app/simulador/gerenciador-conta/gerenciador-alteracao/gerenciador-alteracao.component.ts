import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';

@Component({
  selector: 'app-gerenciador-alteracao',
  templateUrl: './gerenciador-alteracao.component.html',
  styleUrls: ['./gerenciador-alteracao.component.css']
})
export class GerenciadorAlteracaoComponent implements OnInit {

  @Input() usuario;
  @Output() eventEmitter = new EventEmitter();
  constructor(library: FaIconLibrary) { 
    library.addIconPacks(fas, far);
  }

  ngOnInit(): void {
  }

  onSalvar(){
    this.eventEmitter.emit(this.usuario);
  }

}
