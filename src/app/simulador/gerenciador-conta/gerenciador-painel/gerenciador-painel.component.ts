import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/services/usuario.service';
import { Router } from '@angular/router';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-gerenciador-painel',
  templateUrl: './gerenciador-painel.component.html',
  styleUrls: ['./gerenciador-painel.component.css']
})
export class GerenciadorPainelComponent implements OnInit {

  usuario
  constructor(public uService: UsuarioService, library: FaIconLibrary, private router: Router) {
    library.addIconPacks(fas, far);
  }

  ngOnInit(): void {
  }

  onRoute(){
    this.router.navigate(["simulador/gereciador-conta/nome"])
  }

}
