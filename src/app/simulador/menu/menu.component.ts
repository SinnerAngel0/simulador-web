import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/services/usuario.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  usuario;
  constructor(private router : Router, public uService: UsuarioService) { }

  ngOnInit(): void {
  }

  onLogout(){
    localStorage.removeItem("token");
    this.router.navigate(['home']);
  }

  home(){
    this.router.navigate(["simulador/diagnostico/financas"])
  }

  onGerenciar(){
    this.router.navigate(['simulador/gerenciador-conta/painel']);
  }


}
