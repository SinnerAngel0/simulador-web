import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/services/usuario.service';
import { ActivatedRoute, ParamMap } from '@angular/router';

declare var $:any;

@Component({
  templateUrl: './simulador.component.html',
  styleUrls: ['./simulador.component.css']
})
export class SimuladorComponent implements OnInit {

  uService: UsuarioService;
  constructor(private _uService: UsuarioService, private route: ActivatedRoute) {
    this.uService = _uService
  }

  ngOnInit(): void {
    this.uService.GetUserProfile();    
    this.route.url.subscribe(() => {
      $('.tooltip').tooltip('dispose');
      $('.popover').popover("dispose");
    })
  }

  ngOnDestroy() {
    this.uService.usuario = null;
  }

}
